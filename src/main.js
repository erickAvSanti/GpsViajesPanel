// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import BootstrapVue from 'bootstrap-vue'
import VueSweetalert2 from 'vue-sweetalert2'
import datePicker from 'vue-bootstrap-datetimepicker' 
import { library } from '@fortawesome/fontawesome-svg-core'
import VueNumberInput from '@chenfengyuan/vue-number-input'
import { 
	faPlus,
	faArrowUp,
	faArrowDown,
	faTimesCircle,
	faChevronRight,
	faChevronLeft,
	faTimes,
	faEdit,
	faCalculator, 
	faSync,
	faArrowCircleDown,
	faArrowCircleRight,
	faPhone,
	faEnvelope,
	faCircle,
	faCheckCircle,
	faCheck,
	faBirthdayCake, 
	faPalette, 
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import App from './App'
import router from './router'


library.add(faPlus)
library.add(faArrowUp)
library.add(faArrowDown)
library.add(faTimesCircle)
library.add(faChevronRight)
library.add(faChevronLeft)
library.add(faTimes)
library.add(faEdit)
library.add(faCalculator)
library.add(faSync)
library.add(faArrowCircleDown)
library.add(faArrowCircleRight)
library.add(faPhone)
library.add(faEnvelope)
library.add(faCircle)
library.add(faCheckCircle)
library.add(faCheck)
library.add(faBirthdayCake) 
library.add(faPalette) 



Vue.use(BootstrapVue)
Vue.use(datePicker)
Vue.use(VueSweetalert2) 
Vue.use(VueAxios, axios) 
Vue.use(require('vue-moment'))
Vue.component(VueNumberInput.name, VueNumberInput)

Vue.config.productionTip = false

global.jQuery = require('jquery');
global.momentjs = require('moment'); 

global.url_server = "http://localhost:8285";
global.url_server_verify_token = global.url_server+"/api/auth/verify-token";
global.url_server_logout = global.url_server+"/api/auth/logout";
global.url_server_login = global.url_server+"/api/auth/login";
global.url_server_users = global.url_server+"/users"; 
global.url_server_user_roles = global.url_server+"/user-roles"; 
global.url_server_clients = global.url_server+"/clients"; 
global.url_server_clients_contacts = global.url_server+"/clients-contacts"; 
global.url_server_area_a = global.url_server+"/area-a"; 
global.url_server_area_b = global.url_server+"/area-b"; 
global.url_server_area_c = global.url_server+"/area-c"; 
global.url_server_tipo_doc = global.url_server+"/tipo-doc"; 
global.url_server_civil_status = global.url_server+"/civil-status"; 
global.url_server_user_area = global.url_server+"/user-area"; 
global.url_server_vehicle_class = global.url_server+"/vehicle-class"; 
global.url_server_vehicle_use = global.url_server+"/vehicle-use"; 
global.url_server_vehicle_prov_gps = global.url_server+"/vehicle-prov-gps"; 
global.url_server_vehicle_conces = global.url_server+"/vehicle-conces"; 
global.url_server_vehicle_conces_local = global.url_server+"/vehicle-conces-local"; 
global.url_server_vehicle_conces_local_seller = global.url_server+"/vehicle-conces-local-seller"; 
global.url_server_vehicle_prod = global.url_server+"/vehicle-prod"; 
global.url_server_policy_end = global.url_server+"/policy-end"; 
global.url_server_policy_veh_cover = global.url_server+"/policy-veh-cover"; 
global.url_server_policy_veh_status_quota = global.url_server+"/policy-veh-status-quota"; 
global.url_server_policy_health_status_quota = global.url_server+"/policy-health-status-quota"; 
global.url_server_policy_veh_reason_cancel = global.url_server+"/policy-veh-reason-cancel"; 
global.url_server_policy_health_reason_cancel = global.url_server+"/policy-health-reason-cancel"; 
global.url_server_policy_insurance_carrier = global.url_server+"/policy-insurance-carrier"; 
global.url_server_vehicles_brand = global.url_server+"/vehicles-brand"; 
global.url_server_vehicles_model = global.url_server+"/vehicles-model"; 

Vue.component('font-awesome-icon', FontAwesomeIcon)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css' 
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css'
import './assets/css/custom.css'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
