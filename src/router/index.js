import Vue from 'vue'
import Router from 'vue-router'  
import Users from '@/components/Users'
import UserRoles from '@/components/UserRoles'
import Clients from '@/components/Clients'
import Login from '@/components/Login'
import Home from '@/components/Home'
import Areas from '@/components/Areas'
import TipoDoc from '@/components/TipoDoc'
import CivilStatus from '@/components/CivilStatus'
import UserArea from '@/components/UserArea'
import VehicleClass from '@/components/VehicleClass'
import VehicleUse from '@/components/VehicleUse'
import VehicleProvGps from '@/components/VehicleProvGps'
import VehicleConces from '@/components/VehicleConces'
import VehicleProd from '@/components/VehicleProd'
import PolicyEnd from '@/components/PolicyEnd'
import InsuranceCarrier from '@/components/InsuranceCarrier'
import Vehicles from '@/components/Vehicles'
import PolicyVehCover from '@/components/PolicyVehCover'
import PolicyVehStatusQuota from '@/components/PolicyVehStatusQuota'
import PolicyHealthStatusQuota from '@/components/PolicyHealthStatusQuota'
import PolicyVehReasonCancel from '@/components/PolicyVehReasonCancel'
import PolicyHealthReasonCancel from '@/components/PolicyHealthReasonCancel'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/users',
      name: 'Usuarios',
      component: Users
    },
    {
      path: '/userroles',
      name: 'UserRoles',
      component: UserRoles
    },
    {
      path: '/clients',
      name: 'Clientes',
      component: Clients
    },
    {
      path: '/home',
      name: 'Principal',
      component: Home
    },
    {
      path: '/areas',
      name: 'Areas',
      component: Areas
    },
    {
      path: '/tipodoc',
      name: 'TipoDoc',
      component: TipoDoc
    },
    {
      path: '/civilstatus',
      name: 'CivilStatus',
      component: CivilStatus
    },
    {
      path: '/userarea',
      name: 'UserArea',
      component: UserArea
    },
    {
      path: '/vehicleclass',
      name: 'VehicleClass',
      component: VehicleClass
    },
    {
      path: '/vehicleuse',
      name: 'VehicleUse',
      component: VehicleUse
    },
    {
      path: '/vehicleprovgps',
      name: 'VehicleProvGps',
      component: VehicleProvGps
    },
    {
      path: '/vehicleconces',
      name: 'VehicleConces',
      component: VehicleConces
    },
    {
      path: '/vehicleprod',
      name: 'VehicleProd',
      component: VehicleProd
    },
    {
      path: '/policyend',
      name: 'PolicyEnd',
      component: PolicyEnd
    },
    {
      path: '/policyinsurancecarrier',
      name: 'InsuranceCarrier',
      component: InsuranceCarrier
    },
    {
      path: '/vehicles',
      name: 'Vehicles',
      component: Vehicles
    },
    {
      path: '/policyvehcover',
      name: 'PolicyVehCover',
      component: PolicyVehCover
    },
    {
      path: '/policyvehstatusquota',
      name: 'PolicyVehStatusQuota',
      component: PolicyVehStatusQuota
    },
    {
      path: '/policyhealthstatusquota',
      name: 'PolicyHealthStatusQuota',
      component: PolicyHealthStatusQuota
    },
    {
      path: '/policyvehreasoncancel',
      name: 'PolicyVehReasonCancel',
      component: PolicyVehReasonCancel
    },
    {
      path: '/policyhealthreasoncancel',
      name: 'PolicyHealthReasonCancel',
      component: PolicyHealthReasonCancel
    },
  ]
})
